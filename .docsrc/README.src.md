# TLF Scripts
These are scripts I use during development & some day-to-day computer usage. 

## Use at your own risk
This might be helpful to you, but use at your own risk. You may need to review the actual code at points.

## Installation
We'll `git clone` this package and run the install script, which will download our single dependency & setup `~/.bashrc` to point to this library.
1. `cd ~/; mkdir vendor; cd vendor;` or `cd` into another directory for downloaded packages
2. `git clone https://gitlab.com/taeluf/bash/tlf.git`
3. `./install`
4. Either `source ~/.bashrc` or re-launch your terminal

## Basics
- run `tlf help` to see command groups available
- prefix all commands with `tlf` ... yeah
- You'll need a `secrets/general.bash` file INSIDE this repo's files for a lot of the features

### Example secrets/general.bash
```bash
autoload_build_path="/absolute/path/to the autoload builder script"
autoload_exec_path="/absolute/path/to a php autoload script"
apache_user="user_name"
apache_group="group_name"
ssh_keys_folder=~/.ssh2 # I use .ssh2 to stop automatic sshkey setup from happening
dnf_install_path="/absolute/path/to a file that keeps a list of dnf packages to install on new system setup"
dnf_remove_path="/absolute/path/to a file that keeps a list of dnf packages to remove on new system setup"
```
