#!/usr/bin/env bash


## Generate a random string (optional length param)
# @param LENGTH is optional
function random_str(){
    # Why not just use bash for this? Because I already had the PHP function AND it sounded like fun to make them work together
    if [[ -z "$1" && $1 -ge 0 ]];then
        php -r "require('$codeDir/lib/core.php'); echo core_randomstr();"
    else
        php -r "require('$codeDir/lib/core.php'); echo core_randomstr(${1});"
    fi
}
