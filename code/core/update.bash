#!/usr/bin/env bash


##
# Update software
#
function update(){
    run help update
}

##
# update SpaceVim
#
function update_spacevim(){
    cd ~/.SpaceVim/
    bent update
}

##
# Update the local copy of the bash cli (based on current working directory)
#
function update_bashcli(){
    cd "$(pwd)/vendor/bash-cli"
    bent update
}
##
# Update the local copy of the bash cli (based on current working directory)
#
function update_bashcli_composer(){
    cd "$(pwd)/vendor/bash-cli"
    composer update
}

##
# Execute scrawl from bash-cli's composer-installed copy of code-scrawl
#
function update_scrawl_docs(){
    echo "Running from $(pwd)"

    if [[ ! -f "$(pwd)/vendor/bash-cli/vendor/taeluf/code-scrawl/bin/scrawl" ]];then
        tlf update bashcli composer
    fi
    ./vendor/bash-cli/vendor/taeluf/code-scrawl/bin/scrawl
}
