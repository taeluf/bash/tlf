#!/usr/bin/env bash
##
# Image commands (Just compression rn)
#

function image(){
    run help image
}

##
# Convert my personal phone images to about 100kb. Searches for .jpg files in current directory & outputs to compressed/*. Run 'tlf image compress' from the directory containing the images to compress.
#
#
# See https://www.linuxjournal.com/content/watermarking-images-command-line
# See https://www.digitalocean.com/community/tutorials/reduce-file-size-of-images-linux
function image_compress(){
    out="$(pwd)/compressed/"
    if [[ ! -d "$out" ]];then
        mkdir "$out"
    fi;
    for full_name in "$(pwd)"/*.jpg 
    do
        name="$(basename ${full_name})"
        dimensions=$(identify $name | cut -d\  -f3)
        if [[ $dimensions == "4032x3024" ]];then
            convert "$name" -verbose -resize 1344x1008 "$out/$name"
        fi
        if [[ $dimensions == "1008x1344" ]];then
            convert "$name" -verbose -resize 1008x1344 "$out/$name"
        fi
    done
    jpegoptim --size=120k "$out/"*.jpg 
}
