#!/usr/bin/env bash

## 
# Call Git Bent's load_ssh function with the ssh_keys_folder in your secrets/general.bash file
# 
# @tip Load an ssh key (depends upon Git Bent)
function ssh(){
    source "${SCRIPT_ROOT}/secrets/general.bash" 
    bent ssh add "${ssh_keys_folder}"
}
