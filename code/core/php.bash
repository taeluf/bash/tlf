#!/usr/bin/env bash

import_secrets


if [[ -z "$php_version" ]];then
    php_version="php"
fi
# echo "$php_version"
# exit

## 
# run a localhost:port server in the current directory.
function php_server(){
    port=$1
    if [[ "$port" == "" ]];then
        port=3000
    fi

    dir=$(git rev-parse --show-toplevel 2>/dev/null)
    cd "$dir"
    $php_version -S localhost:$port deliver.php
}

##
# Executes the php script at the autoload_build_path set in your secrets/general.bash file
#
# @tip set up the current directory to be autoloaded by other php apps on the system
function php_autoload(){
    source "${SCRIPT_ROOT}/secrets/general.bash"
    $php_version "${autoload_exec_path}"
    # php "${autoload_build_path}"
}

##
# Link to a vendor on my local system, using symlinks
#
# @depends upon my personal fuzzy dir finding script
function php_linkto(){
    # echo "$@"
    dir="$(fuzzy $@)"
    composer="$dir/composer.json"
    name="$(php -r "require('$codeDir/lib/core.php'); echo package_name('$composer');")"
    prompt_yes_or_no "Link $(pwd)/vendor/$name \n  to $dir? " || return

    if [[ -h "$(pwd)/vendor/$name" ]];then
        rm "$(pwd)/vendor/$name"
    elif [[ -d "$(pwd)/vendor/$name" ]];then
        date="$(date +%s)"
        # mv "$(pwd)/vendor/$name" "$(pwd)/vendor/${name}.${date}.bak"
        rm -rf "$(pwd)/vendor/$name"
    elif [[ -s "$(pwd)/vendor/$name" ]];then
        rm "$(pwd/vendor/$name)"
    fi
    ln -s "$dir" "$(pwd)/vendor/$name"

    if [[ -s "$(pwd)/vendor/$name" ]];then
        echo "Linked!"
    else
        echo "Link failed"
    fi
}
