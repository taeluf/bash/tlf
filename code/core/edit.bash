#!/usr/bin/env bash

##
# edit config files
# @usage [tlf linux edit] & answer prompt to enter file
# @usage [tlf linux edit filename] to edit a given file in the homedir (only if it exists). Automatically checks for .filename as well & will use that if exists
#
function edit(){
    if [[ -n "$1" ]];then
        file="$1"
    fi
    echo $file
    if [[ -f ~/"$file" ]];then
        vim ~/"$file"
        return
    elif [[ -f ~/."$file" ]];then
        vim ~/."$file"
        return
    fi
    files=("~/.bashrc" "$(__FILE__)")

     msg_header "Choose file to edit or enter name of file in home dir"

     local -n array_to_choose_from=files
     prompt_choose_from_array answer


     answer=$(expand_tilde_path "$answer")
     echo "$answer"
     vim "$answer"

}

