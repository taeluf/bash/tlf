#!/usr/bin/env bash


## clock in
# @param LENGTH is optional
function clock_in(){

    php -r "require('$codeDir/lib/core.php'); echo core_clockin();"
}

##
# clock out
function clock_out(){
    php -r "require('$codeDir/lib/core.php'); echo core_clockout();"
}
##
# check current clock in/out status
function clock_check(){
    php -r "require('$codeDir/lib/core.php'); echo clock_check();"
}

##
# edit the log of clockins/outs
function clock_edit(){
    vim "$codeDir/cache/clocks.php"
}

