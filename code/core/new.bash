#!/usr/bin/env bash


function new(){
    run new help
}

## 
# run a localhost:port server in the current directory.
function new_project_php(){

    msg_warn "Experimental, unpolished feature likely containing bugs"

    msg_notice "New project in $(pwd)"
    prompt_yes_or_no "Continue? " || return

    msg "1. Make new project: \n  https://gitlab.com/projects/new"
    prompt "New Project URL (https):" https_url
    git_url="$(https_url_to_ssh "$https_url")"

    prompt "initial branch name: " branchName

    git init -b "${branchName}"
    git remote add origin "${git_url}"
    git pull origin "${branchName}"
    git fetch
    
    cp "$(dirname $0)/../code/extra/composer.json" "$(pwd)/composer.json"


    prompt "[enter] to continue & edit composer.json"

    vim "$(pwd)/composer.json"
    # git push --all
    mkdir ./code
    composer update
    ./vendor/taeluf/code-scrawl/bin/scrawl

    echo "vendor/" >> "$(pwd)/.gitignore"

    mkdir ./.docsrc
    mkdir ./.docsrc/sample
    cp -r ./vendor/taeluf/code-scrawl/.docsrc ./.docsrc/sample

    git add .
    git commit -am "init repo"
    git push --all

    msg_instruct "1. Submit to Packagist"
    msg "https url is ${https_url}"
    msg "  https://packagist.org/packages/submit"

    prompt "[enter] to continue"
    msg_instruct "2. Get API Key "
    msg "  https://packagist.org/profile/"

    prompt "[enter] to continue"
    msg_instruct "3. Setup integration"
    msg "  ${https_url}/-/services/packagist/edit"

    msg_header "All done! hopefully"

    bent status

    msg_instruct "do a bent update / git push --all"

}

## Plans for new_project_php
# 1. new project on gitlab (can i prompt for project name & pre-fill gitlab form?)
# 2. git clone it
# 3. initialize code scrawl - config file, directory structure, placeholder readme & placeholder "features file to show everything scrawl can do4. Initialize php tester
# 5. Write deliver files
# 6. Init composer (& gitignore!)
# 7. Go to packagist to submit package
# 8. Go to gitlab for packagist integration
#
# Maybe each of my projects can have a sample folder from whcih contents get copied for a new project setup. Copy happens after composer install, so versioning is managed
