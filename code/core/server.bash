#!/usr/bin/env bash

##
# Work with remote servers
function server(){
    run help server
}

## 
# Setup current directory to host config & setup files for connection to remove server
#
function server_setup(){
    # Confirm directory to write config files
    # Write default excludes file
    # Ask for SSH user, SSH Domain, Remote folder
    # Write file with ssh settings
    # Suggest editing excludes file

    dir="$(get_server_dir)"

    read -p "Setup in dir '${dir}'? " yn
    if [[ "${yn}" != "y" ]]; then
        echo "Answered no...";
        return;
    fi

    excludes="${dir}/excludes"

    echo -e ".git" >> "${excludes}"
    echo "Default excludes appended to '${excludes}'..."

    read -e -p "SSH User: " sshUser;
    read -e -p "SSH Domain: " sshDomain;
    read -e -p "Remote Directory: " remoteDirectory;
    read -e -p "Relative Local Directory: " localDirectory;

    settings="${dir}/settings"
     
    echo "#!/bin/bash" >> "$settings"
    echo "sshUser=\"${sshUser}\"" >> "$settings"
    echo "sshDomain=\"${sshDomain}\"" >> "$settings"
    echo "remoteDirectory=\"${remoteDirectory}\"" >> "$settings"
    echo "localDirectory=\"${localDirectory}\"" >> "$settings"

    echo "settings" >> ".gitignore"

    echo "SSH Settings written to '$settings'";
    echo "You may want to edit the excludes file.";
}

## 
# Download files from remote server
#
function server_pull(){
    dir="$(get_server_dir)"
    dl=$(dirname "$dir")
    file="${1}"

    msg "\nsettings dir:$dir";
    msg "\nproject dir:$dl";
    msg "\nrelative download dir:$file";

    if [[ -z "$file" || ! -e "${dl}/${file}" ]]; then
        read -e -p "Path, relative to '${dl}/':" file
    fi

    #if [[ -z "$file" || ! -e "${dir}/../${file}" ]]; then
    #	echo "${dir}/../${file} does not exist";
    #	return;
    #fi

    source "${dir}/settings"

    ssh="${sshUser}@${sshDomain}:${remoteDirectory}"
    echo ""
    echo "doing dry rsync"

    rsync -avn --exclude-from "${dir}/excludes" "${ssh}/${file}" "${dl}/${file}"



    read -p "Continue with download into '${dl}/${file}'? " yn
    if [[ "${yn}" != "y" ]]; then
        return;
    fi

    rsync -av --exclude-from "${dir}/excludes" "${ssh}/${file}" "${dl}/${file}"
}

## 
# Upload local files to a remote server
# add `remoteTestDirectory="~/pub/test.example.com"` to `.server/settings`
# 
function server_test(){

    dir="$(get_server_dir)"

    read -p "Push from dir '${dir}'? " yn
    if [[ "${yn}" != "y" ]]; then
        echo "Answered no...";
        return;
    fi

    source "${dir}/settings"
    remoteDirectory="$remoteTestDirectory"


    read -p "Push to '${remoteDirectory}'? " yn
    if [[ "${yn}" != "y" ]]; then
        echo "Answered no...";
        return;
    fi
    ssh="${sshUser}@${sshDomain}:${remoteDirectory}"

    rsync --copy-dirlinks -av --exclude-from "${dir}/excludes" --delete "${dir}/${localDirectory}/" "${ssh}"

    ssh "${sshUser}@${sshDomain}" \
    "if [[ -f ${remoteDirectory}/release/post-upload.php ]];then
        echo ""
        echo "Running ${remoteDirectory}/release/post-upload.php"
        echo ""
        source ~/.bashrc
        php ${remoteDirectory}/release/post-upload.php
    fi"
}


## 
# Upload local files to a remote server
function server_push(){

    dir="$(get_server_dir)"

    read -p "Push from dir '${dir}'? " yn
    if [[ "${yn}" != "y" ]]; then
        echo "Answered no...";
        return;
    fi

    source "${dir}/settings"

    ssh="${sshUser}@${sshDomain}:${remoteDirectory}"

    rsync --copy-dirlinks -av --exclude-from "${dir}/excludes" --delete "${dir}/${localDirectory}/" "${ssh}"

    ssh "${sshUser}@${sshDomain}" \
    "if [[ -f ${remoteDirectory}/release/post-upload.php ]];then
        echo ""
        echo "Running ${remoteDirectory}/release/post-upload.php"
        echo ""
        source ~/.bashrc
        php ${remoteDirectory}/release/post-upload.php
    fi"
}

## 
# Connect to remote server and copy site files to backup dir on the server
function server_backup(){
    dir="$(get_server_dir)"

    read -p "Run remote backup from '${dir}'? " yn
    if [[ "${yn}" != "y" ]]; then
        echo "Answered no...";
        return;
    fi

    source "${dir}/settings"

    ssh="${sshUser}@${sshDomain}:${remoteDirectory}"


    ssh "${sshUser}@${sshDomain}" \
    "
    echo '${remoteDirectory}/../backup/'
    if [[ -d ${remoteDirectory}/../backup/ ]];then
        source ~/.bashrc

        backupDir=\"\$(realpath ${remoteDirectory}/../backup/)/$(basename ${remoteDirectory})-$(date +%m-%d-%Y)\"
        echo \"backup up to \$backupDir\" 
        cp -r \"\$(realpath ${remoteDirectory})\" \"\${backupDir}\"
    fi"
}

##
# Connect to remote server and interface over ssh terminal
function server_ssh(){
    dir="$(get_server_dir)"
    read -p "Connect from dir '${dir}'? " yn
    if [[ "${yn}" != "y" ]]; then
        echo "Answered no...";
        return;
    fi
    source "${dir}/settings"

    ssh "${sshUser}@${sshDomain}"
    # ssh "${sshUser}@${sshDomain}" 
    # i need a -t
    #"cd ${remoteDirectory}; bash --login;"
}

## 
# Create an ssh key & upload the public copy to your server"
function server_ssh_key(){
    dir="$(get_server_dir)"

    read -p "Upload SSH key for '${dir}'? " yn
    if [[ "${yn}" != "y" ]]; then
        echo "Answered no...";
        return;
    fi

    source "${dir}/settings"

    read -e -p "SSH Keys Directory (leave blank for ~/.ssh): " keysDir
    read -e -p "Key Name: " keyName

    if [[ -z "$keysDir" ]]; then
        keysDir="$HOME/.ssh"
    fi

    keysDir="$(expand_tilde_path "${keysDir}")"
    
    prompt_yes_or_no "Generate New Key?" yesno
    if [[ $yesno ]];then
        echo "Generating SSH Key"
        ssh-keygen -t ed25519 -f "${keysDir}/${keyName}"
    fi

    echo "Uploading SSH Key"
    ssh-copy-id -i "${keysDir}/${keyName}.pub" "${sshUser}@${sshDomain}"

    echo "Adding Key"
    ssh-add "${keysDir}/${keyName}"
}
