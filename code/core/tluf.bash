#!/usr/bin/env bash

function tluf(){
    import_secrets
    echo "edit $tluf_file"
    nvim "$tluf_file"
    msg_instruct "To push to server: tlf tluf push"
}

function tluf_push(){
    import_secrets
    cd "$tluf_server_dir"
    tlf server push
}
