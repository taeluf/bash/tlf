#!/usr/bin/env bash

# import lib/linux2
import lib/linux3
import lib/linux3backup

## Setup, configure, backup my personal linux systems
#
# Aims to have a single folder to contain all backup-able items


## 
# Set up a new system from my setup source files. Do this AFTER a restore from backup
#
#
function linux3_setup(){
    
    LOADED_CONFIG="false"
    if [[ -f "$(pwd)/.tlflinux" ]];then
        prompt_yes_or_no "Load setup file $(pwd)/.tlflinux" answer
        if [[ $answer ]];then
            source "$(pwd)/.tlflinux"
            LOADED_CONFIG="true"
        fi
    fi

    while [[ ! -d "$home_dir" ]]; do
        prompt_or_default "Absolute path to home directory" "$HOME" home_dir 
    done

    if [[ $LOADED_CONFIG == "false" ]];then
        source "$home_dir/.tlflinux"
        LOADED_CONFIG="true"
    fi

    while [[ ! -d "$data_dir" ]]; do
        prompt_or_default "Absolute path to data directory" "$(pwd)" data_dir
    done

    while [[ ! -d "$setup_dir" ]]; do
        prompt "Relative path to setup dir, inside ${data_dir}" s
        setup_dir="$data_dir/$s"
    done

    if [[ ! -f "$setup_script" ]];then
        setup_script="$setup_dir/setup"
    fi

    while [[ ! -f "$setup_script" ]]; do
        prompt "Relative path to setup script, inside ${setup_dir}" setup_script_rel
        setup_script="$setup_dir/$setup_script_rel"
    done


    trash_dir="$data_dir/trash/$(date +"%y-%M-%d-%H-%M")/"

    msg "Home Dir: $home_dir"
    msg "Data Dir: $data_dir"
    msg "Setup Dir: $setup_dir"
    msg "Setup Script: $setup_script"
    msg "Trash Dir: $trash_dir"

    prompt_enter

    source "$setup_script"
}

##
# backup one dir to another using a config file for settings
#
function linux3_backup(){

    msg "Current Dir: $(pwd)"
    msg_instruct "Use absolute paths"

    while [[ ! -f "$configFile" ]];do
        prompt "Backup Configuration File (N-setup new)" configFile
        if [[ "$configFile" == "N" ]];then
            backup_generate_config           
        fi
        configFile="$(expand_tilde_path "$configFile")"
    done

    msg_notice "Source $configFile. Configs: "
    source "$configFile"
    cat "$configFile"

    prompt_yes_or_no "Backup .git folders?" doBackupGit
    prompt_yes_or_no "Do dry run?" doDryRun

    echo "Backing up from '$sourceDir' into '$backupDir'"



    # preBackupScript="$sourceDir/pre-backup.bash"
    # if [[ -f "$preBackupScript" ]];then
        # step "Run pre backup Script?" source "$preBackupScript"
    # fi


    
    if [[ $doBackupGit ]];then
        msg_notice "compressing git dirs"

        run_compress_git "$sourceDir"

        echo -e "\n\n-------- .git folders compressed: \n"
        find "$sourceDir" | grep ".git.tar.gz"
        echo -e "\n----------------\n"

        read -p "(enter) to continue"
    else
        msg_notice "git not backed up"
    fi


    ## --archive equals -rlptgoD    or:
        # --recursive # recurse into directories
        # --links # copy symlinks as symlinks
        # --perms # preserve permissions
        # --times # preserve modification times
        # --group # preserve group
        # --owner # preserve owner
        # -D is --devices --specials # "preserve device files" & "preserve special files" respectively
    ## Others:
    # --hard-links preserve hard links (probably not needed)
    # --executability preserve executability
    # --acls preserve ACLs (implies --perms) (see `info ACL`. I think this is needed for sticky bits to work? idk)
    rsync_args=(--verbose --archive --hard-links --executability --acls --progress)
    if [[ $doDryRun ]]; then
        rsync_args+=(--dry-run)
        msg_notice "Do dry run!"
    fi

    rsync_end_args=()
    if $useExcludesFile;then
        rsync_end_args+=("--exclude-from=$excludesListFile")
    fi

    echo "Source Dir: $sourceDir"
    echo "Backup Dir: $backupDir"

    if [[ $doDryRun ]];then

        while [[ ! -d "$(dirname "$rsyncLogFile")" || -z "$rsyncLogFile" ]];do
            prompt "Rsync log file" rsyncLogFile
            rsyncLogFile="$(expand_tilde_path "$rsyncLogFile")"
        done
        rsync "${rsync_args[@]}" --delete --delete-before --delete-excluded --exclude=".git/" --exclude="vendor/" "${rsync_end_args[@]}"  "$sourceDir" "$backupDir" > "$rsyncLogFile"
    else
        rsync "${rsync_args[@]}" --delete --delete-before --delete-excluded --exclude=".git/" --exclude="vendor/" "${rsync_end_args[@]}"  "$sourceDir" "$backupDir"
    fi

    if [[ $doBackupGit ]];then
        msg_notice "Decompressing git dirs"
        run_decompress_git "$sourceDir"
    fi

    echo -e "\nShould be all done!!\n"

}
