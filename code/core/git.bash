#!/usr/bin/env bash

##
# Get the git status of all direct sub-directories of the directory this is executed in
# This should be integrated into Git Bent as a much nicer function
# Also would be a good way to find git projects on a system... maybe
#
# @tip (currently disabled) show git status of all sub-directories of current working directory
function git_status(){
    msg_notice "function currently disabled due to help menu parsing issues"
    #find . -maxdepth 1 -mindepth 1 -type d -exec sh -c '(echo {} && cd {} && git -c color.status=always status && echo)' \; | less -R
}
