#!/usr/bin/env bash

##
# verify a hash of a downloaded file. Not yet implemented
#
# @prompt_arg path to iso file
# @prompt_arg hash to verify
# @prompt_arg hash method to use
function hash_verify(){

    msg_notice "This method not implemented ..."

    return;

    msg_notice "only md5sums can be checked right now"

    msg_header "Verify md5sum"
    
    prompt_quit "Path to iso file: " isoFile && return
    while [[ ! -f "$isoFile" ]];do
        msg "'$isoFile' is not a file"
        msg ""
        prompt_quit "Path to iso file: " isoFile && return
    done
    prompt_quit "Hash to confirm against: " correctHashPath && return
    while [[ ! -f "$correctHashPath" ]];do
        msg "'$correctHashPath' is not a file"
        prompt_quit "Hash to confirm against: " correctHashPath && return
    done

    prompt_quit "hash method to use? (md5/sha256)" hashMethod

    if [[ "$hashMethod" == "md5" ]];then
        md5sum -c "$correctHashPath" "$isoFile"
    else
        msg "md5 is the only method available right now"
    fi

}
