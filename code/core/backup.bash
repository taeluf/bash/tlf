#!/usr/bin/env bash

##
# backup a mysql database with mysqldump
function backup_db(){
    msg "Will backup DB into `pwd`";
    prompt_quit "Database name:" database \
        && return;
    prompt_quit "Database user:" user \
        && return;

    date=$(date +%y-%m-%d);
    file="${database}_${date}.sql";
    mysqldump --no-tablespaces -u $user -p $database > "$file"
}
