#!/usr/bin/env bash

##
# Link to a file in the current directory to your links directory. Typically used for the homedir & may have unexpected results otherwise
# @usage tlf linux link - then answer the prompt
#
function linux_link(){
    import_secrets
    msg_header "Files in $(pwd)"
    ls -la
    msg
    msg_notice "You should only run this from $HOME, generally"
    prompt "Name of file to link to: " file_name

    file="$(pwd)/$file_name"
    dest="${linux_link_dir%%\/}/$file_name"

    if [[ -f "$file" || -d "$file" ]]; then
        if [[ -f "$dest" || -d "$dest" ]];then
            mv "$file" "$dest.old.$(date +%s)"
        else
            mv "$file" "$dest"
        fi
    fi
    if [[ ! -f "$dest" && ! -d "$dest" ]];then
        msg_warn "$dest does not exist"
        if prompt_yes_or_no "Make file? (y) or directory? (n)" ;then
            touch "$dest"
        else
            mkdir "$dest"
        fi
    fi
    echo "ln -s $dest $file"
    ln -s "$dest" "$file"
}
