#!/usr/bin/env bash

##
# Sets up a new virtualhost pointing to the current directory on Fedora
#
function apache_vhost(){

    if [[ "$1" == 'all' ]];then
        sudo vim /etc/httpd/conf.d/localserver.conf
        sudo service httpd restart
        return;
    fi

    if [[ "$1" == 'dir' ]];then
        echo "/etc/httpd/conf.d/site"
        ls -la "/etc/httpd/conf.d/site"
        return;
    fi

    if [[ "$1" == 'site' ]];then
        ls -la "/etc/httpd/conf.d/site"
        read -p "Site subdomain: " site
        sudo vim "/etc/httpd/conf.d/site/${site}.localhost.conf"
        return;
    fi

    if [[ "$1" == 'restart' ]]; then
        sudo systemctl restart httpd
        return;
    fi;


    # read -p "Site Name: " siteName
    curDir="$(pwd)"
    echo -e "\n  Make site in:\n    - ${curDir}";
    read -p "\n  Local subdomain: " localSubdomain
    host="${localSubdomain}.localhost"

    declaration="
    <Directory '${curDir}'>
        Options Indexes FollowSymLinks Includes ExecCGI
        AllowOverride All
        Require all granted
    </Directory>
    <VirtualHost ${host}:80>
        ServerName ${host}
        DocumentRoot '${curDir}'
    </VirtualHost>
    "

    # echo "${declaration}";

    # return;

    dir="/etc/httpd/conf.d/site"
    path="${dir}/${host}.conf"
    echo "${declaration}" | sudo tee "${path}"

    echo "  ### Saved at ${path} ###"
    echo "${declaration}"
    echo

    read -p "Type 'd' to delete the file: " doDelete

    if [[ "${doDelete}" == 'd' ]]; then
        sudo rm "${path}"
    fi

    chcon -R -t httpd_sys_content_t "${curDir}"
    sudo service httpd restart
}
