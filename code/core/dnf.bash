#!/usr/bin/env bash

##
# dnf system-setup stuff
#
function dnf(){
    run help dnf
}


##
# Edit the dnf-install file for system setup
function dnf_edit_install(){
    import_secrets
    echo "nvim $dnf_install_path"
    nvim "$dnf_install_path"
}

##
# Edit the dnf-remove file for system setup
function dnf_edit_remove(){
    import_secrets
    echo "nvim $dnf_remove_path"
    nvim "$dnf_remove_path"
}

##
# dnf install a package, then optionally add the dnf-install to system setup files
function dnf_install(){
    import_secrets

    msg_notice "Prompting for sudo for dnf install"
    sudo dnf install $1

    echo "dnf install $1"
    if prompt_yes_or_no "Add package installation to system setup?";then
        echo "$1" >> "$dnf_install_path"
        echo "Added to setup"
    else
        msg_notice "did NOT add to setup"
    fi
    
}

##
# dnf remove a package, then optionally add the dnf-remove to system setup files
#
function dnf_remove(){
    import_secrets

    msg_notice "Prompting for sudo for dnf remove"
    sudo dnf remove $1

    echo "dnf remove $1"
    if prompt_yes_or_no "Add package removal to system setup?";then
        echo "$1" >> "$dnf_remove_path"
        echo "Added to setup"
    else
        msg_notice "did NOT add to setup"
    fi
    
}
