#!/usr/bin/env bash

import core/apache-fedora

##
# apache localhost management
function apache(){
    run help apache
}

## Sets up the current directory as apache user & set permissions & update selinux to run under apache
#
function apache_dir(){

    msg "this function is disabled because I don't need it any more AND the commented line below caused parsing issues when it was uncommented that stopped the help menu generation from working correctly"
    return

    source "${SCRIPT_ROOT}/secrets/general.bash" 

    curDir="$(pwd)"

    read -p "Set dir ${curDir} as apache group? (y/n)" doServer
    if [[ "${doServer}" != "y" ]]; then
        echo "Canceled...";
        return;
    fi

    didSudo="$(sudo pwd)";
    ## sudo find "${curDir}" -type d -exec chmod g+s {} +
    sudo chmod +x "${curDir}/.htaccess"
    sudo chown -R "${apache_user}" "${curDir}"
    sudo chgrp -R "${apache_group}" "${curDir}"

    find $SERVER_PATH -type d -print0 | sudo xargs -0 chmod 771 
    find $SERVER_PATH -type f -print0 | sudo xargs -0 chmod 660
    find $SERVER_PATH -type f -name ".htaccess" -print0 | sudo xargs -0 chmod o+r
    sudo semanage fcontext -a -t httpd_sys_rw_content_t "${curDir}(/.*)?"; 
    sudo restorecon -RF "${curDir}/"
    return;
    ## old code I'm not using any more...

    SERVER_PATH="${curDir}"

    sudo chgrp -R "${apache_group}" $SERVER_PATH
    sudo chown -R "${apache_user}" $SERVER_PATH

    find $SERVER_PATH -type f -name "*.sh" -print0 | sudo xargs -0 chmod ug+x

    sudo chmod -R ug+x $BASH_DIR

}
