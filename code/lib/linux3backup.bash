#!/usr/bin/env bash


function backup_generate_config(){
    configFile=""
    sourceDir=""
    backupDir=""

    msg_notice "Creating Config File"
    msg_instruct "Enter absolute paths"

    # Location to backup sourcedir to
    while [[ ! -d "$(dirname "$configFile")" || -z "$configFile" ]]; do
        prompt "Config File Destination" configFile 
        configFile="$(expand_tilde_path "$configFile")"
    done


    while [[ ! -d "$sourceDir" ]]; do
        prompt "Directory to backup" sourceDir
        sourceDir="$sourceDir/"
    done

    # Location to backup sourcedir to
    while [[ ! -d "$backupDir" ]]; do
        prompt "Backup Destination Dir" backupDir
        backupDir="${backupDir%/}"
    done

    excludesListFile=""
    useExcludesFile=true
    while [[ ! -f "$excludesListFile" ]]; do
        prompt "rsync excludes file (s-skip)" excludesListFile
        if [[ "$excludesListFile" == "s" ]];then
            useExcludesFile=false
            excludesListFile=""
            break;
        fi
    done

    
    echo "#!/usr/bin/env bash" > "${configFile}"
    echo "sourceDir=\"${sourceDir}\"" >> "${configFile}"
    echo "backupDir=\"${backupDir}\"" >> "${configFile}"
    echo "excludesListFile=\"${excludesListFile}\"" >> "${configFile}"
    echo "useExcludesFile=$useExcludesFile" >> "${configFile}"


    msg_instruct "${configFile} saved"
}
