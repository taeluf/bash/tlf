#!/usr/bin/env bash

https_url_to_ssh(){
    echo "$1" | sed -r 's/https:\/\/([^\/]+)\/(.*)/git@\1:\2.git/'
}
