#!/usr/bin/env bash

step_list=()

import_secrets(){
    source "${SCRIPT_ROOT}/secrets/general.bash"
}
##
# @ arg function_name
function step(){
    str="${@:2}"
    step_list+=("$str")
    step_msg+=("$1")
}

function step_run(){
    i=0
    for cmd in "${step_list[@]}";do
        prompt="${step_msg[$i]}"
        i=$((i+1))
        echo "  [n] to skip"
        read -p "$prompt " n
        if [[ "${n:0:1}" == "n" ]];then
            echo "skipped"
            echo ""
        else 
            $cmd
            echo ""
        fi
    done
}

## 
# Make a symlink at HOME_DIR/symlink_name for each file/directory in target directory 
# Does not descend into directories
#
# @arg target_dir the directory to scan
# @arg home_dir the home dir to copy into
# @arg backup_dir the dir to move these to
function write_home_symlinks_to(){
    local target_dir
    target_dir="$1"
    local home_dir
    home_dir="$2"
    backup_dir="$3"

    while IFS= read -r -d $'\0' filePath; do
        if [[ "$filePath" == "$target_dir/" ]]; then
            continue
        fi
        fileName=$(basename "$filePath")
        target_file="$target_dir/$fileName"

        # remove symlink
        if [[ -h "$home_dir/$fileName" ]];then
            rm "$home_dir/$fileName"
        fi
        # move any other file or directory
        if [[ -f "$home_dir/$fileName" || -d "$home_dir/$fileName" ]];then
                mv "$home_dir/$fileName" "$backup_dir/$fileName"
        fi
            ln -s "$target_file" "$home_dir/$fileName"
            echo "Linked ~/$fileName to $target_file";
        
    done < <(find "$target_dir/" -maxdepth 1 -print0)
}


##
# Compress all git dirs 
# Skips all vendor directories
function run_compress_git(){
    sourceDir="$1"

    echo "Looking git repos in '$sourceDir'"

    while IFS= read -r -d $'\0' fpath; do
        if [[ "$fpath" == *"/vendor/"* ]];then
            echo "SKIP: $fpath"
            continue
        fi
        cd "$fpath/.."
        f=$(basename "$fpath")
        echo "$fpath"
        tar -zcf "${f}.tar.gz" "$f"
        # rm -rf "$f"
        #mv "$f" "$f.bak"
        #tar -zxf "${f}.tar.gz"
        #break
    done < <(find "$sourceDir/" -type d -name ".git" -print0)
}

##
# Decompress all compressed git directories
#
function run_decompress_git(){
    sourceDir="$1"

    echo "Looking for git repos in '$sourceDir'"

    while IFS= read -r -d $'\0' fpath; do
        dir=$(dirname "$fpath")
        cd "$dir"
        f=$(basename "$fpath")
        echo "$fpath"
        tar -zxf "${f}"
        rm "${f}"
    done < <(find "$sourceDir/"  -type f -name ".git.tar.gz" -print0)
}
