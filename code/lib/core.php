<?php


/**
 * get the 'name' entry from a composer.json & return it
 */
function package_name($composer_file){
    $comp = json_decode(file_get_contents($composer_file));
    return $comp->name;
}

function core_randomstr(int $len=26){

    for ($chars = array('0123456789','abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ','!@#$%^&*()_+-='),$randomString="",$i=0;$i<$len;$i++)$randomString .= substr($chars[$i%4], random_int(0,strlen($chars[$i%4])), 1);

    return $randomString;
}

function clock_stuff(&$clocks, &$month, &$week, &$day, &$now){
    date_default_timezone_set('America/Chicago');

    $month = date("M-Y");
    $day = date('l');
    $dayofmonth = date("j");
    $dayofmonth = 14;
    $week = $dayofmonth / 7;
    if ($week <= 1)$week = 1;
    elseif ($week<=2)$week = 2;
    elseif ($week<=3)$week = 3;
    elseif ($week<=4)$week = 4;
    else $week = 5;
    $week = "week-$week";

    if ($clocks==null){
        $clocks = include($clockFile=__DIR__.'/../cache/clocks.php');
    }
    if (!is_array($clocks))$clocks = [];

    $now = date("H:i");

}
function clock_put($clocks){
    $clockFile = __DIR__.'/../cache/clocks.php';
    file_put_contents($clockFile, '<?php return '.var_export($clocks, true).';');
}

function core_clockin(){

    clock_stuff($clocks, $month, $week, $day, $now);


    $last_clock = @array_pop($clocks[$month][$week][$day]);
    if (!is_array($last_clock))$last_clock = [];
    if (!isset($last_clock['out'])){
        echo "\nYou are already clocked in.\n";
        return;
    }
    $clocks[$month][$week][$day][] = $last_clock;

    $clocks[$month][$week][$day][] = [
        'in' => $now
    ];

    clock_put($clocks);

    echo "\nClocked in at $now\n";
}

function core_clockout(){

    clock_stuff($clocks, $month, $week, $day, $now);

    $last_clock = @array_pop($clocks[$month][$week][$day]);
    if (!is_array($last_clock))$last_clock = [];
    if (!isset($last_clock['in'])||isset($last_clock['out'])){
        echo "\nCannot clock out because you were not clocked in.\n";
        return;
    }
    $last_clock['out'] = $now;

    if ($last_clock['in']!==$last_clock['out']){
        $clocks[$month][$week][$day][] = $last_clock;
        echo "\nClocked out at $now\n";
    } else {
        echo "\nNo duration. Clock Discarded\n\n";
    }

    clock_put($clocks);



    clock_check($last_clock, $clocks);


}

function clock_duration($clock_entry, &$hours, &$minutes){

    $in = explode(':',$clock_entry['in']);
    $out = explode(':', $clock_entry['out'] ?? date("H:i"));
    $hours = $out[0] - $in[0];
    $minutes = (int)$out[1] - (int)$in[1];

    if ($minutes < 0){
        $hours -= 1;
        $minutes += 60;
    }

    if ($minutes >= 60){
        $minutes -= 60;
        $hours += 1;
    }
}


function clock_check($last_clock=null, $clocks=null){

    clock_stuff($clocks, $month, $week, $day, $now);


    if ($last_clock===null){
        $last_clock = @array_pop($clocks[$month][$week][$day]);
        if (!is_array($last_clock))$last_clock = null;
        // if (!is_array($last_clock))$last_clock = ['in'=>'1:00', 'out'=>'1:00'];
    }
    $todayH = 0;
    $todayM = 0;
    foreach ($clocks[$month][$week][$day] as $clock){
        clock_duration($clock, $h, $m);
        $todayH += $h;
        $todayM += $m;
    }
    while ($todayM>=60){
        $todayM -= 60;
        $todayH += 1;
    }


    // if ($last_clock!=null&&!isset($last_clock['out'])){
        // $last_clock['out'] = $now;
    // }
    clock_duration($last_clock, $nowH, $nowM);
    if (is_array($last_clock)&&!isset($last_clock['out'])&&isset($last_clock['in'])){
        echo "\nCurrently Clocked In\n\n";
    }
    echo "You worked: "
        .($last_clock==null ? '' :"\nNow: $nowH hours, $nowM minutes")
        ."\nToday: $todayH hours, $todayM minutes";




    $weekH = 0;
    $weekM = 0;
    foreach ($clocks[$month][$week] as $day => $clock_list){
        foreach ($clock_list as $clock){
            clock_duration($clock, $h, $m);
            $weekH += $h;
            $weekM += $m;
        }
    }
    while ($weekM>=60){
        $weekM -= 60;
        $weekH += 1;
    }
    echo "\nThis Week: $weekH hours, $weekM minutes";


    $monthH= 0;
    $monthM = 0;
    foreach ($clocks[$month] as $week=>$week_list)
    foreach ($week_list as $day => $clock_list){
        foreach ($clock_list as $clock){
            clock_duration($clock, $h, $m);
            $monthH += $h;
            $monthM += $m;
        }
    }
    while ($monthM>=60){
        $monthM -= 60;
        $monthH += 1;
    }
    echo "\nThis Month: $monthH hours, $monthM minutes";

}
