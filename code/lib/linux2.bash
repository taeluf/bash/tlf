#!/usr/bin/env bash

##
# Prompts for some paths & sources your config file
# sets up $home_dir, $root_dir, and $config_file variables
#
function linux2_init_configs(){
    while [[ ! -d "$homeDir" ]]; do
        prompt_or_default "Absolute path to home directory" "$HOME" homeDir 
    done

    home_dir="$homeDir"
    while [[ ! -d "$rootDir" ]]; do
        prompt_or_default "Absolute path to data dir" "$(pwd)" rootDir
    done
    root_dir="$rootDir"

    echo "$root_dir"
    echo "$rootDir"

    prompt_enter

    msg
    msg_header "Contents of ${rootDir}"
    ls -a "$rootDir"

    while [[ ! -f "$rootDir/$configFile" ]]; do
        line_break
        msg_instruct "Paths relative to ${rootDir}"
        msg "  Enter directory to list contents"
        prompt_or_default "Config File" "config.bash" configFile
        if [[ -d "$rootDir/$configFile" ]]; then
            msg
            msg_header "Contents of ${rootDir}/${configFile}"
            ls -a "$rootDir/$configFile"
        fi
    done

    echo "$config_file"
    config_file="$rootDir/${configFile}"
    configFile="$config_file"

    source "${configFile}"
}

function linux2_setup_homelinks(){

    msg_status "linux2_setup_homelinks"

    ## Setup direct homedir links
    for relPath in "${homeLink[@]}";do
        targetDir="${rootDir}/${relPath}"
        if [[ $(prompt_yes_or_no "Write symlinks to ${targetDir}?") ]];then
            write_home_symlinks_to "$targetDir" "$homeDir" "$trashDir"
        fi
    done
}



function linux2_dnf_remove(){
    if [[ -f "$setup_dir/dnf-remove" ]];then
        dnfRemoveList="$(cat "$setup_dir/dnf-remove")"
        # prompt=$(echo -e "\nRemove ${dnfRemoveList[@]}")
        sudo dnf remove ${dnfRemoveList[@]}
    else 
        msg_notice "Cannot find dnf-remove file at ${setup_dir}/dnf-install"
    fi
}

function linux2_dnf_install(){
    if [[ -f "$setup_dir/dnf-install" ]];then
        dnfInstallList="$(cat "$setup_dir/dnf-install")"
        # prompt=$(echo -e "\nInstall ${dnfInstallList[@]}")
        sudo dnf install ${dnfInstallList[@]}
    else 
        msg_notice "Cannot find dnf-install file at ${setup_dir}/dnf-install"
    fi
}

##
# Link files within your root dir
# @usage rootlink link_location link_target
function rootlink(){
    local link_location="$1"
    local link_target="$2"


    local cur_dir="$(pwd)"
    cd "$home_dir"


    linux2_trash "${root_dir}" "$link_location"
    msg_status "  then: rootlink ln_location:$link_location ln_target:$link_target"
    msg_status "    ln -s \"${link_target}\" \"${root_dir}/$link_location\""
    ln -s "${link_target}" "${root_dir}/$link_location"

    cd "${cur_dir}"
}

## Link home_dir files to root_dir files
# @usage homelink link_location [link_target] 
# @arg link_location is required
# @arg link_target is optional. If not present, link_target="$link_location"
function homelink(){
    local link_location="$1"
    local link_target="$1"


    local cur_dir="$(pwd)"
    cd "$home_dir"

    if [[ -n "$2" ]];then
        link_target="$2"
    fi
    linux2_trash "$home_dir" "$link_location"

    msg_status "  then: homelink ln_location:${link_location} target:${link_target}"
    msg_status "    ln -s \"${root_dir}/${link_target}\" \"${home_dir}/${link_location}\""

    ln -s "${root_dir}/${link_target}" "${home_dir}/${link_location}"

    cd "$cur_dir"
}

## link home_dir files to each file in the given directory
# @usage homelink_children rel_path
# @arg rel_path - a path relative to the $root_dir
function homelink_children(){
    local rel_path="$1"

    local cur_dir="$(pwd)"
    cd "$home_dir"

    msg_instruct "homelink_children $root_dir/$rel_path"
    local f_temp="$(ls -a "${root_dir}/${rel_path}")"
    local files=()
    str_split_line "$f_temp" files
    for f in "${files[@]}"; do
        if [[ "$f" == "." || "$f" == ".." ]];then
          continue
        fi
        local symlink="${home_dir}/${f}"
        local ln_target="$root_dir/$rel_path/$(basename "$f")"
        linux2_trash "${home_dir}" "$f"
        msg_status "  then: child-link $symlink to $ln_target"
        ln -s "$ln_target" "$symlink"
    done

    cd "$cur_dir"
}

## Move a file to the setup trash dir
# @usage linux2_trash /absolute/parent/dir/ relative/path/to/trash
function linux2_trash(){
    local from_dir="$1"
    local target_file="$2"
    local trash_sub_dir="$(dirname "$target_file")"

    msg_status "linux2_trash ${from_dir}/${target_file}"
    if [[ -e "${from_dir}/${target_file}" || -h "${from_dir}/${target_file}" ]];then
        mkdir --parents "$trash_dir/$trash_sub_dir"
        mv "${from_dir}/${target_file}" "${trash_dir}/${target_file}"
    else
        msg_notice "failed to trash because ${from_dir}/${target_file} does not exist"
    fi
}

## Deeply link root_dir files into the home_dir
# @usage homelink_deep "apps/deeplink"
#     This will symlink "$home_dir/i3/config" to "$root_dir/apps/deeplink/i3/config" & do the same for any other regular files found in "$root_dir/apps/deeplink"
function homelink_deep(){
    local rel_path="$1"
    local input_dir_path="${root_dir}/$rel_path"
    # echo "$root_dir"
    # echo "$input_dir_path"
    ### find every file
    local cur_dir="$(pwd)"
    cd "$home_dir"

    msg_instruct "homelink_deep $rel_path"

    while IFS= read -r -d $'\0' filePath; do
        if [[ -d "$filePath" ]];then continue; fi
        # echo "$filePath"
        rel_file_path="${filePath##${input_dir_path}/}"
        target_home_path="${home_dir}/${rel_file_path}"
        linux2_trash "${home_dir}" "$rel_file_path"
        msg_status "mkdir \"$(dirname ${target_home_path})\""
        mkdir --parents "$(dirname ${target_home_path})"
        msg_status "  then: ln -s ${filePath} ${target_home_path}"
        ln -s "${filePath}" "${target_home_path}"
    done < <(find "$input_dir_path"/ -type f -print0)

    cd "$cur_dir"
}
