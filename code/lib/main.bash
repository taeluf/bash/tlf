#!/usr/bin/env bash


##
# Get the path to the server dir
# either $(pwd) or $(pwd)/.server
# @usage dir="$(get_server_dir)"
#
function get_server_dir(){
    local dir;
    dir="$(pwd)"

    if [[ -d "${dir}/.server" ]];then
        dir="$(pwd)/.server"
    fi

    echo "$dir"
}
