prompt_choose_function "# transcribe [command]${cOff}-"  \
"${help_mode} " "-default-" "Transcribe audio using OpenAI's whisper. Ex: tlf transcribe audio-file.mp4. Run tlf makemd afterward." \
"${help_mode} cleantsv" "cleantsv" "Clean a .tsv file into a human-readable form. You probably just should use makemd, though." \
"${help_mode} makemd" "makemd" "Create a .md file with proper disclaimer header and links to timestamped portions of the uploaded video. Ex: tlf transcribe makemd audio-file.mp4 youtube.com/video_id 0"