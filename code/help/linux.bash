prompt_choose_function "# linux [command]${cOff}-"  \
"${help_mode} link" "link" "Link to a file in the current directory to your links directory. Typically used for the homedir & may have unexpected results otherwise" \
"${help_mode} setup" "setup" "Set up a new system from my setup source files. Do this AFTER a restore" \
"${help_mode} restore" "restore" "Restore backup files from backup drive onto system drive" \
"${help_mode} backup" "backup" "Run a backup from system drive to external drive"