prompt_choose_function "# server [command]${cOff}-"  \
"${help_mode} " "-default-" "Work with remote servers" \
"${help_mode} setup" "setup" "Setup current directory to host config & setup files for connection to remove server" \
"${help_mode} pull" "pull" "Download files from remote server" \
"${help_mode} test" "test" "Upload local files to a remote server" \
"${help_mode} push" "push" "Upload local files to a remote server" \
"${help_mode} backup" "backup" "Connect to remote server and copy site files to backup dir on the server" \
"${help_mode} ssh" "ssh" "Connect to remote server and interface over ssh terminal" \
"${help_mode} ssh key" "ssh key" "Create an ssh key & upload the public copy to your server"
