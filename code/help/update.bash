prompt_choose_function "# update [command]${cOff}-"  \
"${help_mode} " "-default-" "Update software" \
"${help_mode} spacevim" "spacevim" "update SpaceVim" \
"${help_mode} bashcli" "bashcli" "Update the local copy of the bash cli (based on current working directory)" \
"${help_mode} bashcli composer" "bashcli composer" "Update the local copy of the bash cli (based on current working directory)" \
"${help_mode} scrawl docs" "scrawl docs" "Execute scrawl from bash-cli's composer-installed copy of code-scrawl"