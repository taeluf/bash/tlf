prompt_choose_function "# php [command]${cOff}-"  \
"${help_mode} server" "server" "run a localhost:port server in the current directory." \
"${help_mode} autoload" "autoload" "Executes the php script at the autoload_build_path set in your secrets/general.bash file" \
"${help_mode} linkto" "linkto" "Link to a vendor on my local system, using symlinks"