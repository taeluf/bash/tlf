prompt_choose_function "# clock [command]${cOff}-"  \
"${help_mode} in" "in" "clock in" \
"${help_mode} out" "out" "clock out" \
"${help_mode} check" "check" "check current clock in/out status" \
"${help_mode} edit" "edit" "edit the log of clockins/outs"