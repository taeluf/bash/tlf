prompt_choose_function "# [command]${cOff}-"  \
"${help_mode} apache" "apache" "apache localhost management" \
"${help_mode} backup" "backup" "-- no description" \
"${help_mode} clock" "clock" "-- no description" \
"${help_mode} core" "core" "does nothing" \
"${help_mode} dnf" "dnf" "dnf system-setup stuff" \
"${help_mode} edit" "edit" "edit config files" \
"${help_mode} extra" "extra" "disorganized stuff i might use sometimes" \
"${help_mode} git" "git" "-- no description" \
"${help_mode} hash" "hash" "-- no description" \
"${help_mode} help" "help" "-- no description" \
"${help_mode} linux" "linux" "-- no description" \
"${help_mode} linux2" "linux2" "-- no description" \
"${help_mode} new" "new" "-- no description" \
"${help_mode} php" "php" "-- no description" \
"${help_mode} random" "random" "-- no description" \
"${help_mode} server" "server" "Work with remote servers" \
"${help_mode} ssh" "ssh" "Call Git Bent's load_ssh function with the ssh_keys_folder in your secrets/general.bash file" \
"${help_mode} tluf" "tluf" "-- no description" \
"${help_mode} update" "update" "Update software" \
"${help_mode} ocr" "ocr" "Convert pdf files to plain text" \
"${help_mode} linux3" "linux3" "-- no description" \
"${help_mode} transcribe" "transcribe" "Transcribe audio using OpenAI's whisper. Ex: tlf transcribe audio-file.mp4. Run tlf makemd afterward." \
"${help_mode} image" "image" "Image commands (Just compression rn)"