prompt_choose_function "# dnf [command]${cOff}-"  \
"${help_mode} " "-default-" "dnf system-setup stuff" \
"${help_mode} edit install" "edit install" "Edit the dnf-install file for system setup" \
"${help_mode} edit remove" "edit remove" "Edit the dnf-remove file for system setup" \
"${help_mode} install" "install" "dnf install a package, then optionally add the dnf-install to system setup files" \
"${help_mode} remove" "remove" "dnf remove a package, then optionally add the dnf-remove to system setup files"