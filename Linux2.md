# Linux 2 - linux system setup / backup / restore


## Nov 8, 2021 notes
Issues:
- homedir links keep pointing to /home/reed/data/config/file
- For each dir in `/home/reed/data`, I'm creating a symlink inside that dir
- This seems to be happening for each homelink_children



## Old (as ovf nov 8, 2021)

## Quick Bits
- I'm a bit unsure where things are
- The file structure i want exists on the kde install on my nvme drive
- I can use a virtual machine to try out the new setup
- I don't know how to test these functions, really. Since I'm working with symlinks its kind of tricky.
- I'm not sure how to transition from the current system I have to the new system.
- I didn't take good notes last time I was working on this & I want better notes now.

## TODO
- write `linux2_default_configs` function
- Add docblocks to my linux2 library functions
- document configs
- document intended file structure
- review linux2 lib code & finish it (there are a few blocks of comments that need programmatically filled)
- figure out how to unit test individual functions??

## Configs
make a `config.bash` file to use for configurations. 

### From Defaults / Prompted for (don't put it in your config.bash)
(should I have a `dataDir`? (leaning no))
- `homeDir` string, absolute path: The directory you're trying to set up, generally on a new system.
- `rootDir` string, absolute path: The directory containing your config file, trash dir, and all your other setup files & scripts. You may have data files outside this directory, and your other configs may use `../` relative paths to get to these data files
- `configFile` string, relative path in `$rootDir`: path to your config file

### Userspace Configs
**NOTICE:** Most of these details are right, but my sample `config.bash` (in a private repo) needs reviewed so I can update the descriptions here.


All file/directory paths are relative to `$rootDir`
- `trashDir` string: A dir to put existing files that will be replaced by symlinks. A dated dir will be created inside this dir to backup those files
- `homeLink=("dir1/" "dir2/")` array: Scan each directory for files & sub-dirs. A same-named symlink will be written to the homedir for each file/subdir found. You may use `..` to reference parent directories. 
    - Ex: `dir1/somefile` will be linked to at `~/somefile`. 
    - Ex2: `dir1/.config/whatever` will NOT be linked to. It would link `~/.config` to `dir1/.config`, not find the descendant file
- `deepHomeLink=("deep1/" "deep2/sub/")` array: Each directory will be recursively crawled. For every non-directory file found, it's parent directories will be created in the home dir, followed by a symlink to the specific file. 
    - Ex: for file `deep1/.config/i3/config`, dirs `~/.config/i3/` will be created, and `~/.config/i3/config` will symlink to `$rootDir/deep1/.config/i3`. 
    - Ex2: For `deep2/sub/configs/test`, since `deep2/sub/` is configured, `~/configs/` will be created, and  `~/configs/test` will symlink to `$rootDir/deep2/sub/configs/test`.
- `directLink=()` array: Direct links give a path for the a symlink to reside at and the path for the symlink to point to
    - Structure: `directLink+=("path in home directory" "path in files directory")`
    - Ex: `directLink+=(".mozilla/firefox/profile.Reed" "files/Firefox/profile.Reed")`. This will create a symlink at `~/.mozilla/firefox/profile.Reed` pointing to `$rootDir/files/Firefox/profile.Reed`
    - Ex2: `directLink+=(".var/app/com.valvesoftware.Steam/" "files/Poly Bridge 2/")`. Create a symlink at `~/.var/app/com.valvesoftware.Steam` pointing to `$rootDir/files/Poly Bridge 2/`
    - Ex3: `directLink+=("bin/" "bin/")` points `~/bin/` at `$rootDir/bin/`
- `exportPaths=()` array: Adds the directory to `~/.bashrc`. 
    - Ex: `exportPaths=("bin/" "bin2/")` will add two lines to `~/.bashrc`, adding `$rootDir/bin/` & `$rootDir/bin2/` to `PATH`. The lines added to `~/.bashrc` are `export PATH="/path/to/root/dir/bin/:$PATH"` & `export PATH="/path/to/root/dir/bin2/:$PATH"`
- `exportChildPaths=()` array: `source`s each file in the given directory by adding a loop to your `~/.bashrc`
    - Ex: `exportChildPaths=("bin_functions/")`. Will add `for f in "/root/dir/bin_functions"/*;do source "$f"; done` to your `.bashrc`. So if you have a file `bin_functions/main.bash` containing functions `happy(){echo "happy"}` & `sad(){echo "sad"}`, then `main.bash` will be `source`d & both `happy` & `sad` will be functions available to you in any terminal you launch with that `.bashrc`. 
    - If you use `binSourceDirs=("bin_functions/" "bin_functions2/")`, then two `for` loops wil be made to source files in both of those dirs.
- `setupDir` string: Dir for system setup files. Ex: `setupDir="setup/"`. The dir `$rootDir/setup/` may contain the following:
    - `dnf-install`: packages to install during system setup
    - `dnf-remove`: packages to remove during system setup
    - `pre-setup.bash`: script to run after configs are loaded, before setup is done
    - `post-setup.bash`: script to run after setup is done
    - `pre-backup.bash`: script to run before backups are done
    - `post-backup.bash`: script to run after backups are done
    - `pre-restore.bash`: script to run before backups are restored
    - `post-restore.bash`: script to run after backups are restored
    - Each of these could have their name changed through a config ... but I don't think I'm going that route for v2.
- `sourceDir` string: a directory containing bash files which you MAY `source` in your setup scripts.
    - Ex: Write `source "$sourceDir"/some-script.bash` in your `pre-setup.bash` script to load in some functions or whatever.
    - Note: You can put any kind of file in here. The point is to provide an easy way for your scripts to access files they need.
    - OLD IDEA: The previous idea was `Dir of bash files which should all be sourced`, but I'm not sure when they would be sourced if that's the case.
- `rsyncExcludes` string: a file to list excludes for `rsync --exclude-from="$file"`, the utility used for running backups.
    - Ex: `rsyncExcludes="backup-exclude/"` will use `$rootDir/backup-exclude` as the `--exclude-from` file

Add any other configs you want, which you'll be using in your setup scripts.  
You may also source other files if you want your configs to be composed from multiple files for organizational purposes.

## Notes
- (probably) before config file is loaded, script will `cd "$rootDir"`, so you can `source whatever/file` & it will `source $rootDir/whatever/file` seamlessly


### Linux2 function
in core/linux2.bash
```
linux2_default_configs
linux2_init_configs

step "Run your pre-setup script?" linux2_user_pre_setup

step "Setup homedir symlinks?" linux2_setup_homelinks
step "Setup deep symlinks within homedir?" linux2_setup_deeplinks
step "Setup direct symlinks?" linux2_setup_directlinks
step "run dnf remove?" linux2_dnf_remove
step "run dnf install?" linux2_dnf_install

step "Run your post-setup script?" linux2_user_post_setup

step_run
```
I could definitely edit this to make it more versatile, more robust, etc ... but I think for now, the best thing is to just roll with what's there & lean on pre-setup & post-setup to get everything else done
