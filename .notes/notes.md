# Releasing a new version of the website

## replace non-standard collation settings
```bash
    latestfile=SQL_FILE.sql; #CHANGE THE FILE NAME!!!!!
    cd /PATH_TO_SITE_DIR/db/
    sed -i 's/utf8mb4_0900_ai_ci/utf8mb4_unicode_ci/g' $latestfile
    sed -i 's/utf8mb4_unicode_520_ci/utf8mb4_unicode_ci/g' $latestfile
```



# Update the database from backup file  
 
```bash
cd ~/pub/DOMAIN.COM/db/; # or whatever directory
mysql -u USER_NAME -h mysql.domain.com -p db_name < NEWEST_BACKUP.sql
```

## Test the staging site
    - Is there a sitemap.xml?
    - are SEO verification files present?
    - Do all the pages work?
    - Does a non-existent url show a nice error page?


# Log into the database
 mysql -u USER_NAME -p -h mysql.DOMAIN.com DATABASE_NAME 
