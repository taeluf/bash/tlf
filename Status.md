# TLf lib development status / notes

## June 28, 2023: tlf image

## Nov 30, 2022: tlf linux3
- linux3 backup was added. it's basically the same as `linux backup`, except it creates & uses a configuration file. It also now will write a dry run to a log file.

## Sep 10, 2022: tlf linux3
- I made MAJOR progress on `linux3 setup` command. This comes with a new structure for the setup directory (formerly the `system` directory). The system directory in my personal `projects/system/Fedora36i3wm` uses the new setup. Everything is working for the `setup` part, except i have not yet implemneted the symlinks portion. I've broken apart the file and setup the arguments in the `execute_symlinks()` function, but I have not proceeded with any of the actual symlinking or implementing the options
- `code/core/linux3.bash` is basically good to go (for `setup`)
- `code/lib/linux3.bash` is where most development is taking place
- I want to add `backup` and `restore` to `code/core/linux3.bash`
- Once `linux3` is done, i want to get rid of `linux` and `linux2`

## OCt 21, 2021
- I started the `new project php` function in `new.bash`. We'll get there.

## Development
- cd into `.config`, then `composer install`
- then run `.config/vendor/taeluf/code-scrawl/bin/scrawl` to generate docs & help menus

## Bugs
- code scrawl (well, the lexer) mis-reads files when there's a `{}`. This is an issue in `apache.bash` & `git.bash` that both use `{}` as part of commands. 

## Next
- Fix my clock in / clock out script. It apparently doesn't work...

## Latest (newest to oldest)
- add `edit` function to edit bashrc file & others more easily
- add `https_url_to_ssh` internal function to convert git urls
- add `tlf update spacevim` and `tlf update bashcli`
- added `.bin/scrawl` to easily exec code scrawl
- got code scrawl working!
- Implement latest cli library (from git bent)
- add `php` group with `tlf php server [port_number]`
- add `step` & `step_run` functions (to internal/linux.bash, but it could be used by anything)
- Improve linux commands
- Add linux backup, setup, and restore
- clockin/out quality of life improvements
- Add clock in / clock out / clock check / clock edit
- update phpautoload to new setup

## Ideas
- More small convenience commands
    - configure nemo dirs, i3, spacevim, other things
    - setup the brightness keybind (needed for i3)
    - create a .desktop file
    - Just... replace current cli workflows with tlf commands (I think i mostly have)
