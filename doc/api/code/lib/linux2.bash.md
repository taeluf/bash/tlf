# File code/lib/linux2.bash
## Functions
- `linux2_init_configs`: 
- `linux2_pre_setup`: 
- `linux2_setup_homelinks`: 
- `linux2_setup_deeplinks`: symlink to all files within this directory - do not symlink to any directories; instead, make parent directories as needed
- `linux2_setup_directlinks`: setup deep links
- `linux2_dnf_remove`: symlink homedir path to target file path
- `linux2_dnf_install`: 
- `linux2_user_post_setup`: 
