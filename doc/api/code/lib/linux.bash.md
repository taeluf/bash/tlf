# File code/lib/linux.bash
## Functions
- `import_secrets`: 
- `step`: @ arg function_name
- `step_run`: 
- `write_home_symlinks_to`: Make a symlink at HOME_DIR/symlink_name for each file/directory in target directory 
Does not descend into directories
- `run_compress_git`: Compress all git dirs 
Skips all vendor directories
- `run_decompress_git`: Decompress all compressed git directories
