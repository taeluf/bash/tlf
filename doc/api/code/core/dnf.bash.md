# File code/core/dnf.bash
## Functions
- `dnf`: dnf system-setup stuff
- `dnf_edit_install`: Edit the dnf-install file for system setup
- `dnf_edit_remove`: Edit the dnf-remove file for system setup
- `dnf_install`: dnf install a package, then optionally add the dnf-install to system setup files
- `dnf_remove`: dnf remove a package, then optionally add the dnf-remove to system setup files
