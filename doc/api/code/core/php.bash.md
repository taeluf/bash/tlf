# File code/core/php.bash
## Functions
- `php_server`: run a localhost:port server in the current directory.
- `php_autoload`: set up the current directory to be autoloaded by other php apps on the system
