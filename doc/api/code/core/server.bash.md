# File code/core/server.bash
## Functions
- `server`: Work with remote servers
- `server_setup`: Setup current directory to host config & setup files for connection to remove server
- `server_pull`: Download files from remote server
- `server_push`: Upload local files to a remote server
- `server_backup`: Connect to remote server and copy site files to backup dir on the server
- `server_ssh`: Connect to remote server and interface over ssh terminal
- `server_ssh_key`: Create an ssh key & upload the public copy to your server"
