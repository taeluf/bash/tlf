# File code/core/update.bash
## Functions
- `update`: Update software
- `update_spacevim`: update SpaceVim
- `update_bashcli`: Update the local copy of the bash cli (based on current working directory)
- `update_bashcli_composer`: Update the local copy of the bash cli (based on current working directory)
- `update_scrawl_docs`: Execute scrawl from bash-cli's composer-installed copy of code-scrawl
