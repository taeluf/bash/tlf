# File code/core/linux.bash
## Functions
- `linux_setup`: Set up a new system from my setup source files. Do this AFTER a restore
- `linux_restore`: Restore backup files from backup drive onto system drive
- `linux_backup`: Run a backup from system drive to external drive
