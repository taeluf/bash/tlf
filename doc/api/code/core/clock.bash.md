# File code/core/clock.bash
## Functions
- `clock_in`: clock in
- `clock_out`: clock out
- `clock_check`: check current clock in/out status
- `clock_edit`: edit the log of clockins/outs
